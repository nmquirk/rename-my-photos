package renamemyphotos;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Nicholas
 */
public class MainController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(MainController.class.getName());

    @FXML
    private TextField inputDirectoryPathTextField;

    @FXML
    ListView renamedDirectoryListView;

    ObservableList<String> files = FXCollections.observableArrayList();

    ObservableList<String> renamedFiles = FXCollections.observableArrayList();

    @FXML
    private void previewFiles(ActionEvent event) {
        renamedFiles.clear();
        generateNewFiles("previewFiles");
    }

    @FXML
    private void executeRename(ActionEvent event) {
        generateNewFiles("executeRename");
        files.clear();
        renamedFiles.clear();
        inputDirectoryPathTextField.setText("");
    }

    private void generateNewFiles(String operation) {

        List<String> previewResults = new ArrayList<String>();

        String previousFieldName = "";
        String fileName = "";

        try {

            for (Object o : files) {
                fileName = (String) o;
                File originalFile = new File(fileName);

                ImageMetadata fileMetadata = null;

                try {
                    fileMetadata = Imaging.getMetadata(new File(fileName));
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.log(Level.SEVERE, "Exception", e.getStackTrace());
                }

                TiffField field = null;
                if (fileMetadata != null && fileMetadata instanceof JpegImageMetadata) {
                    JpegImageMetadata jpegMetadata = (JpegImageMetadata) fileMetadata;
                    field = jpegMetadata.findEXIFValueWithExactMatch(ExifTagConstants.EXIF_TAG_DATE_TIME_DIGITIZED);
                }

                String fieldName;
                if (field == null || field.getValueDescription() == null) {
                    fieldName = previousFieldName;
                } else {
                    fieldName = field.getValueDescription();
                    fieldName = fieldName.replace(":", "");
                    fieldName = fieldName.replace(" ", "T");
                    fieldName = fieldName.replace("'", "");
                    previousFieldName = fieldName;
                }

                File newDirectory = new File(originalFile.getParent(), "_Renamed_Photos");
                newDirectory.mkdir();

                File copiedFile = new File(newDirectory.getAbsolutePath(), fieldName + "-" + originalFile.getName());

                if (operation.equals("executeRename")) {
                    FileUtils.copyFile(originalFile, copiedFile);
                } else {
                    previewResults.add(copiedFile.getAbsolutePath());
                }
            }

            if (previewResults.size() > 0) {
                renamedFiles.addAll(previewResults);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE, "Exception", e.getStackTrace());
        }
    }

    @FXML
    private void loadFiles(ActionEvent event) {

        Predicate<String> isPictureFile = item
                -> item.toLowerCase().endsWith("tif")
                || item.toLowerCase().endsWith("tiff")
                || item.toLowerCase().endsWith("jpg")
                || item.toLowerCase().endsWith("jpeg");

        File folder = new File(inputDirectoryPathTextField.getText());
        File[] listOfFiles = folder.listFiles();

        List<String> results = new ArrayList<String>();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                results.add(file.getAbsolutePath());
            }
        }

        List<String> filterdResult = results.stream().filter(isPictureFile).collect(Collectors.toList());
        files.clear();
        files.addAll(filterdResult);
    }

    @FXML
    private void handleExitAction(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        renamedDirectoryListView.setItems(renamedFiles);
        
        inputDirectoryPathTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            loadFiles(null);
            previewFiles(null);
        });
    }
}
