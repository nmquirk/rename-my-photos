package renamemyphotos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class RenameMyPhotos extends Application {

    private static Scene scene;

    public static Scene getScene() {
        return scene;
    }

    @Override
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));

        scene = new Scene(root);

        stage.setScene(scene);
        stage.setWidth(700);
        stage.setHeight(600);
        stage.setTitle("Rename My Photos");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
